const express = require('express')
const app = express()
const port = process.env.PORT; // => Port as long belom dipake, ini bebas.

// Ini untuk memberitahu express, kalo kita menerima Content-Type berupa application/json
app.use(express.json())

/*
 * Ketika client request ke server, ada yang namanya Header.
 * Content-Type: text/html, application/json, text/plain
 * */

/*
 * Web API, Web Server, Server Side Application.
 * - HTTP
 * - WS
 * Browser, Mobile Apps, Aplikasi Desktop, ini yang kita sebagai Client Side Application.
 * Berkomunikasi dengan server menggunakan protocol HTTP.
 *
 * Postman. Tolong install.
 * */

/*
 *
 * HTTP Request:
 * - Header (Kop surat)
 *   - Informasi User, siapa yang request
 *   - Request apa, Mozilla Firefox
 * - Body (Isi Surat)
 *   - Judul buku
 *   - Penulis buku
 *   - Penerbit buku
 *
 * HTTP Request Method
 *
 * Ga punya body di dalam request object
 * - GET    => Ndapetin suatu data. Ngambil data buku.
 * - DELETE => Delete, menghapus buku dari database.
 *
 * Punya body di dalam request object
 * - POST   => Create, ngebuat data, contoh nambah Buku ke database.
 * - PUT    => Update, ngedit buku yang udah ada di database.
 * */

/*
 * RESTful API, atau REST API.
 * Client => /books => GET /api/v1/books
 * RESTful API.
 * Setiap request yang memiliki body, sangat dianjurkan berupa JSON.
 * Setiap response yang memiliki body, harus banget pake JSON.
 * */

let books = [
  {
    id: 1,
    title: 'Sapiens',
    author: 'Yuval Noah'
  }
]

const createBook = body => {
  const book = {
    id: (books[books.length - 1].id || 0) + 1, // Ini untuk Generate ID, karena ga pake DB.
    title: body.title,
    author: body.author
  }
  books.push(book)
  return book
}

const findBook = id => {
  return books.find(i => i.id == id)
}

app.get('/', (req, res) => {
  res.json({
    message: 'Hello World!'
  })
})

// app.post('/sum', (req, res) => {
//   const { x, y } = req.body;
//   const result = x + y;
//   res.status(200).json({
//     parameters: req.body,
//     operation: 'sum',
//     result 
//   })
// })

app.get('/books', (req, res) => {
  res.status(200).json({
    books
  })
})

// POST /books
app.post('/books', (req, res) => {
  const book = createBook(req.body)
  res.status(201).json({
    book
  })
})

// GET /books/1
app.get('/books/:id', (req, res) => {
  const book = findBook(req.params.id) 
  res.status(200).json({
    book
  })
})

// const sebuahArray = [1,2,3,4,5,6]
// console.log(sebuahArray)
// sebuahArray[2] = 5
// console.log(sebuahArray)
app.put('/books/:id', (req, res) => {
  // Cari index buku dulu
  // Terus replace dengan request body, tapi jangan replace id
  const bookIndex = books.findIndex(i => i.id == req.params.id)
  if (bookIndex < 0) return res.status(404).json({
    message: 'Book not found!'
  })

  let book = books[bookIndex]
  let updatedBook = {
    id: book.id,
    title: req.body.title || book.title,
    author: req.body.author || book.author
  }

  books[bookIndex] = updatedBook
  res.status(200).json({
    book: updatedBook
  })
})

app.delete('/books/:id', (req, res) => {
  // Cari index buku dulu
  // Terus replace dengan request body, tapi jangan replace id
  const bookIndex = books.findIndex(i => i.id == req.params.id)

  books.splice(bookIndex, 1)
  res.status(204).end()
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
